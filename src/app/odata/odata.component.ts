import { Component, OnInit } from '@angular/core';
import { DataManager, ODataAdaptor } from '@syncfusion/ej2-data';

@Component({
  selector: 'app-odata',
  templateUrl: './odata.component.html',
  styleUrls: ['./odata.component.scss']
})
export class OdataComponent implements OnInit {

  public data: DataManager;

    ngOnInit(): void {
        this.data = new DataManager({
            url: 'https://js.syncfusion.com/demos/ejServices/Wcf/Northwind.svc/Orders?$top=7',
            adaptor: new ODataAdaptor()
        });
    }

}
