import { Component, OnInit } from '@angular/core';
import { RuleModel } from '@syncfusion/ej2-angular-querybuilder';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  public data: Object[];
    public importRules: RuleModel;
    public values: string[] = ['Mr.', 'Mrs.'];
    ngOnInit(): void {
        this.data =  [{
          'EmployeeID': 1,
          'LastName': 'Davolio',
          'FirstName': 'Nancy',
          'Title': 'Sales Representative',
          'TitleOfCourtesy': 'Ms.',
          'Date': '12/10/2018',
          'Address': '507 - 20th Ave. E.\r\nApt. 2A',
          'City': 'Seattle',
          'Region': 'WA',
          'PostalCode': '98122',
          'Country': 'USA'
        },
        {
          'EmployeeID': 2,
          'LastName': 'Fuller',
          'FirstName': 'Andrew',
          'Title': 'Vice President',
          'TitleOfCourtesy': 'Dr.',
          'Date': '22/06/2018',
          'Address': '908 W. Capital Way',
          'City': 'Tacoma',
          'Region': 'WA',
          'PostalCode': '98401',
          'Country': 'USA'
        },
        {
          'EmployeeID': 3,
          'LastName': 'Leverling',
          'FirstName': 'Janet',
          'Title': 'Sales Representative',
          'TitleOfCourtesy': 'Ms.',
          'Date': '12/10/2011',
          'Address': '722 Moss Bay Blvd.',
          'City': 'Kirkland',
          'Region': 'WA',
          'PostalCode': '98033',
          'Country': 'USA'
        },
        {
          'EmployeeID': 4,
          'LastName': 'Peacock',
          'FirstName': 'Margaret',
          'Title': 'Sales Representative',
          'TitleOfCourtesy': 'Mrs.',
          'Date': '01/11/2014',
          'Address': '4110 Old Redmond Rd.',
          'City': 'Redmond',
          'Region': 'WA',
          'PostalCode': '98052',
          'Country': 'USA'
        },
        {
          'EmployeeID': 5,
          'LastName': 'Buchanan',
          'FirstName': 'Steven',
          'Title': 'Sales Manager',
          'TitleOfCourtesy': 'Mr.',
          'Date': '12/10/2018',
          'Address': '14 Garrett Hill',
          'City': 'London',
          'Region': null,
          'PostalCode':
          'SW1 8JR',
          'Country': 'UK'
        },
        {
          'EmployeeID': 6,
          'LastName': 'Suyama',
          'FirstName': 'Michael',
          'Title': 'Sales Representative',
          'TitleOfCourtesy': 'Mr.',
          'Date': '01/12/2018',
          'Address': 'Coventry House\r\nMiner Rd.',
          'City': 'London',
          'Region': null,
          'PostalCode': 'EC2 7JR',
          'Country': 'UK'
        },
        {
          'EmployeeID': 7,
          'LastName': 'King',
          'FirstName': 'Robert',
          'Title': 'Sales Representative',
          'TitleOfCourtesy': 'Mr.',
          'Date': '12/12/2011',
          'Address': 'Edgeham Hollow\r\nWinchester Way',
          'City': 'London',
          'Region': null,
          'PostalCode': 'RG1 9SP',
          'Country': 'UK'
        },
        {
          'EmployeeID': 8,
          'LastName': 'Callahan',
          'FirstName': 'Laura',
          'Title': 'Inside Sales Coordinator',
          'Date': '12/10/2012',
          'TitleOfCourtesy': 'Ms.',
          'Address': '4726 - 11th Ave. N.E.',
          'City': 'Seattle',
          'Region': 'WA',
          'PostalCode': '98105',
          'Country': 'USA'
        },
        {
          'EmployeeID': 9,
          'LastName': 'Dodsworth',
          'FirstName': 'Anne',
          'Title': 'Sales Representative',
          'TitleOfCourtesy': 'Ms.',
          'Date': '12/03/2018',
          'Address': '7 Houndstooth Rd.',
          'City': 'London',
          'Region': null,
          'PostalCode': 'WG2 7LT',
          'Country': 'UK'
        }];
        this.importRules = {
        'condition': 'and',
        'rules': [{
                'label': 'Employee ID',
                'field': 'EmployeeID',
                'type': 'number',
                'operator': 'equal',
                'value': 1
            },
            {
                'label': 'Title',
                'field': 'Title',
                'type': 'string',
                'operator': 'equal',
                'value': 'Sales Manager'
            }]
        };
    }

}
