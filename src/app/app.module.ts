import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { 
  GridModule,
  PageService, 
  SortService, 
  FilterService, 
  GroupService, 
  SearchService, 
  ToolbarService 
} from '@syncfusion/ej2-angular-grids';

import { QueryBuilderModule } from '@syncfusion/ej2-angular-querybuilder';
import { ScheduleModule } from '@syncfusion/ej2-angular-schedule';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteModule } from '@syncfusion/ej2-angular-dropdowns';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { EJ_DIALOG_COMPONENTS } from 'ej-angular2/src/ej/dialog.component'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GridComponent } from './grid/grid.component';
import { CalendarComponent } from './calendar/calendar.component';
import { FormComponent } from './form/form.component';
import { ReportComponent } from './report/report.component';
import { PivotComponent } from './pivot/pivot.component';
import { OdataComponent } from './odata/odata.component';
import { enableRipple } from '@syncfusion/ej2-base';

@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    CalendarComponent,
    FormComponent,
    ReportComponent,
    PivotComponent,
    OdataComponent,
    EJ_DIALOG_COMPONENTS 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GridModule,
    ScheduleModule,
    FormsModule, 
    ReactiveFormsModule, 
    AutoCompleteModule, 
    DropDownListModule,
    QueryBuilderModule
  ],
  providers: [
    PageService,
    SortService,
    FilterService,
    GroupService,
    SearchService, 
    ToolbarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
