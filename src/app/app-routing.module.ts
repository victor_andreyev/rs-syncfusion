import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GridComponent } from './grid/grid.component';
import { CalendarComponent } from './calendar/calendar.component';
import { FormComponent } from './form/form.component';
import { ReportComponent } from './report/report.component';
import { PivotComponent } from './pivot/pivot.component';
import { OdataComponent } from './odata/odata.component';

const routes: Routes =  [
  { path: 'grid', component: GridComponent },
  { path: 'calendar', component: CalendarComponent },
  { path: 'form', component: FormComponent },
  { path: 'report', component: ReportComponent },
  { path: 'pivot', component: PivotComponent },
  { path: 'odata', component: OdataComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
